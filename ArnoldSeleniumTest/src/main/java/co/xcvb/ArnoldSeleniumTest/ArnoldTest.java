package co.xcvb.ArnoldSeleniumTest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Test the Arnold Challenge website.
 * 
 * @author Calum Gardiner
 *
 */
public class ArnoldTest implements Test {

	// Browserstack selenium server details
	public static final String USERNAME = "calumgardiner1";
	public static final String AUTOMATE_KEY = "jtxgcN3rMZGVVp5yd5yM";
	public static final String URL = "http://" + USERNAME + ":" + AUTOMATE_KEY
			+ "@hub.browserstack.com/wd/hub";
	
	private Capability capability;
	private DesiredCapabilities caps;
	
	public ArnoldTest(Capability capability) {
		this.capability = capability;
	}
	
	public void setupTest()  {
		// set the capabilities to test
		caps = new DesiredCapabilities();
	    caps.setCapability("browser", capability.getBrower());
	    caps.setCapability("browser_version", capability.getBrowserVersion());
	    caps.setCapability("os", capability.getOs());
	    caps.setCapability("os_version", capability.getOsVersion());
	    caps.setCapability("browserstack.debug", "true");
	}
	
	public void test() {
		// set the test behviour
	    WebDriver driver;
		try {
			driver = new RemoteWebDriver(new URL(URL), caps);
			driver = new Augmenter().augment(driver);
		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		    driver.get("http://arnold.xcvb.co/");
		    
		    // enter the details into the form and submit it
		    WebElement registration = driver.findElement(By.name("reg"));
		    WebElement stockReference = driver.findElement(By.name("stockref"));
		    registration.sendKeys("FD12UXE");
		    stockReference.sendKeys("ARNEU-U-8718");
		    stockReference.submit();
		    
		    // wait for content to load then take a screenshot
		    driver.findElement(By.className("smallimg"));
		    ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		    
		    // click smallimg
		    WebElement firstImage = driver.findElement(By.id("0"));
		    firstImage.click();
		    ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		    
		    // quit the test
		    driver.quit();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

}
