package co.xcvb.ArnoldSeleniumTest;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriverException;

public class ExecuteTests {

	public static void main(String args[]) {
		ExecuteTests tests = new ExecuteTests();
		tests.runTests();
	}

	private List<Capability> capabilities;

	public ExecuteTests() {
		this.capabilities = new ArrayList<Capability>();

		// Windows capabilities
		Capability cap = new Capability("IE", "7.0", "Windows", "XP");
		this.capabilities.add(cap);

		cap = new Capability("IE", "9.0", "Windows", "7");
		this.capabilities.add(cap);

		cap = new Capability("IE", "10.0", "Windows", "8");
		this.capabilities.add(cap);

		cap = new Capability("Firefox", "30.0", "Windows", "7");
		this.capabilities.add(cap);

		cap = new Capability("Safari", "5.1", "Windows", "7");
		this.capabilities.add(cap);

		cap = new Capability("Chrome", "35.0", "Windows", "7");
		this.capabilities.add(cap);

		cap = new Capability("Opera", "12.16", "Windows", "7");
		this.capabilities.add(cap);

		// OSX
		cap = new Capability("Safari", "7.0", "OS X", "Mavericks");
		this.capabilities.add(cap);

		cap = new Capability("Firefox", "30.0", "OS X", "Mavericks");
		this.capabilities.add(cap);

		cap = new Capability("Chrome", "35.0", "OS X", "Mavericks");
		this.capabilities.add(cap);
		
		// this capability seems to be timing out on browserstacks server.
		//cap = new Capability("Opera", "12.15", "OS X", "Mavericks");
		//this.capabilities.add(cap);

		
	}

	public void runTests() {
		System.out.println("Now Running tests... this may take a while.");
		for (Capability cap : capabilities) {
			System.out.println("Testing: " + cap.getOs() + " "
					+ cap.getOsVersion() + " - " + cap.getBrower() + " "
					+ cap.getBrowserVersion());
			Test test = new ArnoldTest(cap);
			test.setupTest();
			try {
				test.test();
			} catch (WebDriverException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Tests complete");
	}

}
