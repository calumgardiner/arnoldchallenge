package co.xcvb.ArnoldSeleniumTest;

/**
 * 
 * Test interface.
 * 
 * @author Calum Gardiner
 *
 */
public interface Test {

	/**
	 * Setup a test
	 */
	public void setupTest();
	
	/**
	 * Run the test
	 */
	public void test();
}
