package co.xcvb.ArnoldSeleniumTest;

public class Capability {
	
	private String brower;
	private String browserVersion;
	private String os;
	private String osVersion;
	
	
	public Capability(String browser, String browserVersion, String os, String osVersion) {
		this.setBrower(browser);
		this.setBrowserVersion(browserVersion);
		this.setOs(os);
		this.setOsVersion(osVersion);
	}


	public String getBrower() {
		return brower;
	}


	public void setBrower(String brower) {
		this.brower = brower;
	}


	public String getBrowserVersion() {
		return browserVersion;
	}


	public void setBrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
	}


	public String getOs() {
		return os;
	}


	public void setOs(String os) {
		this.os = os;
	}


	public String getOsVersion() {
		return osVersion;
	}


	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

}
