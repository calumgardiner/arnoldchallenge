<?php
include "ArnoldCar.php";

/**
 * Car finder script.
 */

if (isset($_POST['reg']) && isset($_POST['stockref'])) {
	
	$registration = $_POST['reg'];
	$stockReference = $_POST['stockref'];
	$car;
	
	try {
		$car = new ArnoldCar($registration, $stockReference);
	} catch (Exception $e) {
		outputMessage("Please enter a valid Registration and Stock Reference.");
		exit;
	}
	
	$images = $car->getImageUrls();
	
	if ($images != null) {
		$i = 0;
		foreach ($images as $smallImage => $bigImage) {
			outputImage($smallImage, "smallimg", $i);
			outputImage($bigImage, "bigimg", $i);
			$i++;
		}
	} else {
		outputMessage("Sorry no images were found, are you sure that car exists?");
	}
}
function outputMessage($message) {
	print '<span class="message">' . $message . '</span>';
}

function outputImage($url, $class, $id) {
	print '<img id="' . $id . '" class="'. $class . '" src="' . $url . '" />';
}
?>