<?php

/**
 * Car.php
 * 
 * Interface describes the expected behaviour of a car.
 *
 * @author Calum Gardiner
 */
interface Car {
	
	/**
	 * Return the registration of the car
	 */
	public function getRegistration();
	
	/**
	 * Return the stock reference of the car
	 */
	public function getStockReference();
	
	/**
	 * Return the obfuscated-stock-reference
	 */
	public function getObfuscatedStockReference();
	
	/**
	 * Return the image urls for this car
	 */
	public function getImageUrls();
	
}
?>