<?php
include "Car.php";
/**
 * ArnoldCar.php
 * 
 * Implementation of the Car interface which uses the arnold clark api.
 * 
 * @author Calum Gardiner
 */
class ArnoldCar implements Car {
	
	private $registration;
	private $stockReference;
	private $obfuscatedStockReference;
	private $images;
	
	
	private $small = 350;
	private $large = 800;
	private $cameras = array("f", "i", "r", "4", "5", "6");
	private $apiUrl = "http://imagecache.arnoldclark.com/imageserver/";
	
	
	function __construct($registration, $stockReference) {
		$this->registration = $registration;
		$this->stockReference = $stockReference;
		if ($registration == null || strlen($registration) == 0) {
			throw new Exception("Registration null or empty.");
		}
		if ($stockReference == null || strlen($stockReference) < 9) {
			throw new Exception("Stock Reference null or empty.");
		}
		$this->generateObfuscatedStockReference();
		$this->generateImageUrls();
	}
	
	/**
	 * Generates the obfuscated code by reversing the registration then
	 * interleaving this with the stock reference and ending on the 9th
	 * character of the stock referece.
	 */
	private function generateObfuscatedStockReference() {
		$reverseReg = strrev($this->registration);
		$obfuscated = "";
		for ($i = 0; $i < strlen($reverseReg); $i++) {
			$obfuscated .= $this->stockReference[$i];
			$obfuscated .= $reverseReg[$i];
		}
		$obfuscated .= $this->getNinethChar($this->stockReference);
		$this->obfuscatedStockReference = $obfuscated;
	}
	
	/**
	 * Returns the 9th alpha-numeric character of a string.
	 */
	private function getNinethChar($str) {
		$i = 0;
		foreach (str_split($str) as $char) {
			if ($i == 8) {
				return $char;
			}
			if (ctype_alnum($char)) {
				$i++;
			}
		}
	}
 	
	public function getRegistration() {
		return $this->registration;
	}
	
	public function getStockReference() {
		return $this->stockReference;
	}
	
	public function getObfuscatedStockReference() {
		return $this->obfuscatedStockReference;
	}
	
	public function getImageUrls() {
		return $this->images;
	}
	
	/**
	 * Generates the image urls for each camera type and checks the urls for 404's.
	 */
	private function generateImageUrls() {
		foreach ($this->cameras as $camera) {
			$smallImageUrl = $this->generateImageUrl($this->small, $camera);
			if ($this->remoteImageExists($smallImageUrl)) {
				$largeImageUrl = $this->generateImageUrl($this->large, $camera);
				$this->images[$smallImageUrl] = $largeImageUrl;
			}
		}
	}
	
	/**
	 * Checks if the remote image exists using the curl library to only download
	 * the http header.
	 */
	private function remoteImageExists($imageUrl) {
		$ch = curl_init($imageUrl);
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_exec($ch);
		$retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		return ($retcode == 200);
	}
	
	private function generateImageUrl($size, $camera) {
		$url = "";
		$url .= $this->apiUrl;
		$url .= $this->obfuscatedStockReference;
		$url .= '/' . $size;
		$url .= '/' . $camera;
		$url .= '/';
		return $url;
	}
	
}
//$car = new ArnoldCar("TN07FVP", "ARNEB-U-11404");
?>