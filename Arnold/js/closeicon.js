// script to make the close icon hover over big images.
var $close = $("#close");
$(".bigimg").mouseenter(function() {
	$close.stop(true, true).animate({opacity: 0.5}, 500);
	$close.fadeIn("fast");
}).mouseleave(function() {
	$close.stop(true, true).animate({opacity: 0}, 0);
});