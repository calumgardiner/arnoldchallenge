// moves the big gallery images forward or back in the order.
function getNumberOfImages() {
	var max = 6, count = 0;
	for (i = 0; i < max; i++) {
		if($("#".concat(i)).length != 0) {
			count  = i;
		}
	}
	return count;
}

$("#left").click(function() {
	$(".bigimg").filter(function() {
		return $(this).css("visibility") == "visible";
	}).previousImage();
});

$("#right").click(function() {
	$(".bigimg").filter(function() {
		return $(this).css("visibility") == "visible";
	}).nextImage();
});

$.fn.previousImage = function () {
	var currentId = $(this).attr("id");
	var nextId, max = getNumberOfImages();
	if (currentId == 0) {
		nextId = max;
	} else {
		nextId = currentId-1;
	}
	var currentBigimg = "#".concat(currentId, ".bigimg");
	var nextBigImg = "#".concat(nextId, ".bigimg");
	$(currentBigimg).css("visibility", "hidden");
	$(nextBigImg).css("visibility", "visible");
	return this;
};

$.fn.nextImage = function () {
	var currentId = $(this).attr("id");
	var nextId, max = getNumberOfImages();
	if (currentId == max) {
		nextId = 0;
	} else {
		nextId = currentId-0+1;
	}
	var currentBigimg = "#".concat(currentId, ".bigimg");
	var nextBigImg = "#".concat(nextId, ".bigimg");
	$(currentBigimg).css("visibility", "hidden");
	$(nextBigImg).css("visibility", "visible");
	return this;
};