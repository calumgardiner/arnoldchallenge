// attach javascript to handle zoom
$(".smallimg").click(function() {
	$(".bigimg").css("visibility", "hidden");
	var id = $(this).attr("id");
	var bigimg = "#".concat(id, ".bigimg");
	$(bigimg).css("visibility", "visible");
	$("#left, #right").stop(true, true).animate({opacity: 0.5}, 500);
	$(".bigimg").click(function () {
		$(this).css("visibility", "hidden");
		$("#close, #left, #right").stop(true, true).animate({opacity: 0}, 0);
	});
});