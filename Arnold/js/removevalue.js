// this script removes the default value from the form input
// when the user clicks on the input
jQuery(document).ready(function($) {
	$(".in").focus(function() {
		var currentValue = $(this).attr("value");
		if (currentValue == "Registration" || currentValue == "Stock Reference") {
			$(this).attr("value", "");
		}
	});
	$(".in").focusout(function() {
		if ($(this).attr("value") == "") {
			if ($(this).attr("name") == "reg") {
				$(this).attr("value", "Registration");
			}
			if ($(this).attr("name") == "stockref") {
				$(this).attr("value", "Stock Reference");
			}
		}
	});
});