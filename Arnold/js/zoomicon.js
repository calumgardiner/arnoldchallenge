// script to make the zoom icon hover over small images.
var $zoom = $("#zoom");
$(".smallimg").mouseenter(function() {
	$zoom.stop(true, true).animate({opacity: 0.5}, 500);
	var offset = $(this).offset();
	var left = offset.left;
	var top = offset.top;
	$zoom.css("top", top-90);
	$zoom.css("left", left+300);
	$zoom.fadeIn("fast");
}).mouseleave(function() {
		$zoom.stop(true, true).animate({opacity: 0}, 500);
});