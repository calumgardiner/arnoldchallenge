// this script submits the form details and returns the results
// using ajax
jQuery(document).ready(function($) {
	$("#search").submit(function(event) {
	
	$("#left, #right").unbind();
	
		// stop all current animations on the results div
		// and fade it out fast
		$("#results").stop(false, true).fadeOut("fast");
		// display loading gif
		$("#loading").css("visibility", "visible");
		
		// prevent normal submit
		event.preventDefault();
		
		// get the input values and submit url
		var $form = $(this),
		regi = $form.find("input[name='reg']").val(),
		ref = $form.find("input[name='stockref']").val(),
		url = $form.attr("action");
		
		// send data
		var posting = $.post(url, {reg: regi, stockref: ref});
	 
		// when posting done
		posting.done(function(data) {
			// set loading gif to hidden
			$("#loading").css("visibility", "hidden");
			// output results
			$("#results").empty().append(data);
			$("#results").css("visibility", "visible");
			$("#results").stop(false, true).fadeIn("fast");
			
			// attached the zoom behviour scripts now
			// that the elements are on the page
			$.getScript( "js/zoom.js" );
			$.getScript( "js/zoomicon.js" );
			$.getScript( "js/closeicon.js" );
			$.getScript( "js/gallery.js" );
		});
	});
});