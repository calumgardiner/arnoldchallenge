<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Arnold Clark Image Search</title>
		<!--  Reset CSS   -->
		<link rel="stylesheet" type="text/css" href="css/reset.css" />
		<!--  Main CSS    -->
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<!--  JQuery      -->
		<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<!-- Fonts 	      -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css' />
		<!-- Scripts      -->
		<script type="text/javascript" src="js/removevalue.js"></script>
		<script type="text/javascript" src="js/ajaxsubmit.js"></script>
	</head>
	<body>
	<div id="content">
	<div id="navigation">
		<div id="logo">
		</div>
		<div class="icon" id="zoom"></div>
		<div class="icon" id="close"></div>
		<div class="icon" id="left"></div>
		<div class="icon" id="right"></div>
		<div id="searchbar">
			<form id="search" action="./api/CarFinder.php" method="post">
			<fieldset>
				<input class="in" type="text" name="reg" value="Registration"/>
				<input class="in" type="text" name="stockref" value="Stock Reference"/><br/>
				<button class="ui-button" type="submit">Search</button>
			</fieldset>
		</form>
		</div>
	</div>
		<div>
			<img id="loading" src="img/ajax-loader.gif" />
		</div>
		<div id="results">
		
		</div>
	</div>
	</body>
</html>