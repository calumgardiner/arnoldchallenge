<?php
include "api/ArnoldCar.php";

class ArnoldCarTest extends PHPUnit_Framework_TestCase {
	
	public function testGenerateObfuscatedStockReference() {
		$car = new ArnoldCar("SO06DNV", "ARNFH-U-5728");
		$this->assertEquals("AVRNNDF6H0-OUS2", $car->getObfuscatedStockReference());
	}
	
	/**
	 * @expectedException Exception
	 */
	public function testWrongFormatRegistration() {
		$car = new ArnoldCar("", "ARNFH-U-5728");
	}
	
	/**
	 * @expectedException Exception
	 */
	public function testWrongFormatStockReference() {
		$car = new ArnoldCar("SO06DNV", "km234");
	}
	
	public function testGetRegistration() {
		$car = new ArnoldCar("SO06DNV", "ARNFH-U-5728");
		$this->assertEquals("SO06DNV", $car->getRegistration());
	}
	
	public function testGetStockReference() {
		$car = new ArnoldCar("SO06DNV", "ARNFH-U-5728");
		$this->assertEquals("ARNFH-U-5728", $car->getStockReference());
	}
	
	public function testGetImageUrls() {
		//this needs some sort of mocking library to mock server responses.
	}
}
?>